@extends('layouts.app')
@section('fond', 'compte')
@section('content')


<div class="container-fluid">
    <div class="card-deck m-5">
        <div class="card mb-6 p-3 centre text-white">

        @if ($amis)
        @foreach ($utilisateurs as $user)
        <h1>Compte de {{ $user->pseudo }}</h1>
         @endforeach
        <p>Vous pouvez discuter avec cette personne</p>
        <a href="{{ route('amis-supprimer', $user->pseudo ) }}" class="text-danger">Supprimer</a>
        @else
            {{-- Vérifie si $demandeEnCours est true ou false suivant si une demande d'amis est déjà en cours --}}
        @if ($demandeEnCours)

            @foreach ($utilisateurs as $user)
                <h1>Compte de {{ $user->pseudo }}</h1>
            @endforeach

            <p>Demande en cours</p>

        @else

            @foreach ($utilisateurs as $user)

                @if ($user->pseudo == Auth::user()->pseudo)
                    <h1>Compte de {{ $user->pseudo }}</h1>

                    <a href="{{ route('compte',  $user->id) }}" class="btn btn-primary">Voir mon compte</a>
                @else
                    <h1>Compte de {{ $user->pseudo }}</h1>

                    <a href="{{ route('ajouter.amis',  $user->id) }}" class="btn btn-primary">Ajouter en amis</a>
                @endif

            @endforeach

        @endif
    @endif
        </div>
    </div>
</div>

@endsection
