@extends('layouts.app')
@section('fond', 'compte')
@section('content')

<?php $i = 1; ?>
<div class="container my-5">
    {{ $users->links() }}
    <table class="table table-striped table-secondary">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Pseudo</th>
            <th scope="col">Email</th>
            <th scope="col">Éditer</th>
            <th scope="col">Supprimer</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
          <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td>{{ $user->pseudo}}</td>
            <td>{{ $user->email}}</td>
            <td><a href="#" class="btn btn-primary">Éditer</a></td>
            <td><a href="{{ route('users-delete', $user->id) }}" id="supprimerUserAdmin" class="btn btn-danger">Supprimer</a></td>
          </tr>
          <?php $i++; ?>
          @endforeach
        </tbody>
      </table>
      {{ $users->links() }}
</div>


@endsection
