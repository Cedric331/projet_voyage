@extends('layouts.app')
@section('fond', 'register')
@section('content')

<div class="container formPays p-3">
<form action="{{ route('creation-pays') }}" method="POST">
    @csrf
    <div class="form-group">
        <label for="name">Nom du Pays:</label>
        <input type="text" class="form-control" id="name" name="name">
    </div>

    <div class="form-group">
        <label for="prix">Prix Moyen du Pays:</label>
        <input type="text" class="form-control" id="prix" name="prix">
    </div>

    <div class="form-group">
        <label for="imagePrincipal">Image Principal</label>
        <input type="file" class="form-control-file" name="imagePrincipal" id="imagePrincipal">
    </div>

    <div class="form-group">
        <div>
            <label for="descriptionPrincipal">Description du Pays:</label>
        </div>
        <textarea name="descriptionPrincipal" id="descriptionPrincipal" cols="60" rows="10"></textarea>
    </div>

    <div class="form-group">
        <div>
            <label for="descriptionClimat">Climat du Pays:</label>
        </div>
        <textarea name="descriptionClimat" id="descriptionClimat" cols="60" rows="10"></textarea>
    </div>

    <div class="form-group">
        <div>
            <label for="descriptionPartir">Quand partir:</label>
        </div>
        <textarea name="descriptionPartir" id="descriptionPartir" cols="60" rows="10"></textarea>
    </div>

    <div class="form-group">
        <label for="imageTourisme1">Image Tourisme 1 :</label>
        <input type="file" class="form-control-file" name="imageTourisme1" id="imageTourisme1">
    </div>

    <div class="form-group">
        <label for="titreTourisme1">Titre Tourisme 1 :</label>
        <input type="text" class="form-control-file" name="titreTourisme1" id="titreTourisme1">
    </div>


    <div class="form-group">
        <div>
            <label for="descriptionTourisme1">Description Tourisme 1 :</label>
        </div>
        <textarea name="descriptionTourisme1"  id="descriptionTourisme1" cols="60" rows="10"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Créer</button>
</form>
</div>

@endsection
