@extends('layouts/app')
@section('fond', 'accueil')
@section('content')


<!-- Formulaire -->
<section class="container cadre w-50">
   <h2>Indiquez vos préférences</h2>
   <form method="GET" action="">
      <div class="form-group">
         <label for="exampleFormControlSelect1">Votre mois de départ:</label>
         <select class="form-control" id="exampleFormControlSelect1" name="mois">
            <option value="null">-- Pas de préférence --</option>
            <option value="Janvier">Janvier</option>
            <option value="Fevrier">Février</option>
            <option value="Mars">Mars</option>
            <option value="Avril">Avril</option>
            <option value="Mai">Mai</option>
            <option value="Juin">Juin</option>
            <option value="Juillet">Juillet</option>
            <option value="Aout">Août</option>
            <option value="Septembre">Septembre</option>
            <option value="Octobre">Octobre</option>
            <option value="Novembre">Novembre</option>
            <option value="Decembre">Décembre</option>
         </select>
      </div>
      <div class="form-group">
         <label for="exampleFormControlSelect2">Choix du continent</label>
         <select multiple class="form-control" id="exampleFormControlSelect2" name="continent">
            <option value="null">-- Aucun choix --</option>
            <option value="Europe">Europe</option>
            <option value="Afrique">Afrique</option>
            <option value="Asie">Asie</option>
            <option value="AmeriqueNord">Amérique du Nord</option>
            <option value="AmeriqueSud">Amérique du Sud</option>
            <option value="Oceanie">Océanie</option>
         </select>
      </div>
      <div class="form-group">
         <label for="formControlRange">Prix moyen/personne</label>
         <input type="range" class="form-control-range" id="formControlRange" name="prix">
      </div>
      <button type="submit" class="btn btn-light">Recherche</button>
   </form>
</section>

@endsection
