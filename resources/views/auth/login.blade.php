@extends('layouts.app')
@section('fond', 'login')
@section('content')


<div class="container cadre w-25">

    <h2 class="centre">Connexion</h2>
    @error('email')
    <span class="invalid-feedback d-inline" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
    @error('password')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <label for="email" class="col-md-6 col-form-label">{{ __('Adresse email : ') }}</label>
        <div class="col-md-12 centre">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                value="{{ old('email') }}" required autocomplete="email" autofocus>
        </div>

        <label for="password" class="col-md-6 col-form-label ">{{ __('Mot de passe : ') }}</label>
        <div class="col-md-12 centre">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                name="password" required autocomplete="current-password">
        </div>

        <div class="form-group row mt-4 mb-0 centre">
            <div class="mt-2 centre col-md-12">
                <button type="submit" class="btn btn-primary">
                    {{ __('Connexion') }}
                </button>
            </div>
            <div class="mt-2 col-md-12 centre">
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Mot de passe oublié ?') }}
                </a>
            </div>
            <div class="col-md-12 centre">
                <a class="btn btn-link" href="{{ route('register') }}">
                    {{ __('Pas encore de compte ?') }}
                </a>
            </div>
        </div>
    </form>

</div>

@endsection
