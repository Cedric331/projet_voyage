@extends('layouts.app')
@section('fond', 'compte')
@section('content')



<div class="container-fluid">
<!-- Card deck -->
<div class="card-deck m-5">

    <!-- Card -->
    <div class="card mb-6 p-3 centre text-white">
      <!--Card image-->
        @if (Auth::user()->is_admin == 1)
            <div><h1 class="card-title font-weight-bold">Compte Administrateur</h1></div>
            <hr>
            <a class="w-50 btn btn-primary"href="{{ route('creer-pays') }}">Voir les Pays</a>
            <hr>
            <a class="w-50 btn btn-primary"href="{{ route('users') }}">Voir les utilisateurs</a>
            <hr>
            <a class="w-50 btn btn-primary"href="{{ route('compte-email') }}">Modifier mon adresse email</a>
            <hr>
            <a class="w-50 btn btn-primary" href="{{ route('password.update') }}">Modifier mon mot de passe</a>
            <hr>
            <form action="{{ route('compte.supprimer', Auth::user()->id) }}" method="POST" class="w-50 centre">
              @csrf
              @method('DELETE')
              <button type="submit" class=" btn btn-danger" value="Supprimer">Supprimer</button>
            </form>
      </div>

        @else

        <div>
            <h1 class="card-title font-weight-bold">Information du compte</h1></div>
            <hr>
            <p class="mb-2 pseudo"> Bienvenue {{ Auth::user()->pseudo }}</p>
            <hr>
            <a class="w-50 btn btn-primary" href="{{ route('compte-amis') }}">Voir mes amis</a>
            <hr>
            <p class="card-text">{{ Auth::user()->email }}</p>
            <a class="w-50 btn btn-primary"href="{{ route('compte-email') }}">Modifier mon adresse email</a>
            <hr>
            <a class="w-50 btn btn-primary" href="{{ route('password.update') }}">Modifier mon mot de passe</a>
            <hr>
            <form action="{{ route('compte.supprimer', Auth::user()->id) }}" method="POST" class="w-50 centre">
              @csrf
              @method('DELETE')
              <button type="submit" class=" btn btn-danger" value="Supprimer">Supprimer</button>
            </form>
      </div>
      @endif


      <div class="card mb-6 p-3 centre text-white">
        <div><h1 class="card-title font-weight-bold">Avatar du compte</h1></div>
        <hr>
                                        {{-- si l'utilisateur à un avatar, c'est l'avatar qui s'affiche sinon son nom --}}
                                        <?php
                                        $user = Auth::user();

                                        $oldfilename = $user->avatar;

                                        $oldfileexists = Storage::disk('public')->exists($oldfilename);
                                        ?>
                                        @if ($oldfileexists)
                                        <div><img src="storage/{{ Auth::user()->avatar }}" alt="avatar du profil" class="avatarCompte"></div>
                                        @else
                                        {{ __('Aucun avatar') }}
                                        @endif
        <hr>
        <form action="{{ route('compte.avatar', $user->id)}}" method="POST" class="w-50 centre row" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="md-form mt-3">
                <label for="avatar" class=" col-form-label text-md-right">{{ __('Modifier mon avatar :') }}</label>
                <input id="avatar" type="file" class="form-control" name="avatar" aria-describedby="avatarHelp">
            </div>
            <button class="btn btn-primary mt-4" type="submit">Mettre à jour</button>
        </form>
      </div>
  </div>

</div>

@endsection
