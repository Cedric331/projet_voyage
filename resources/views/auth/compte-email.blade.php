@extends('layouts.app')
@section('fond', 'compte')
@section('content')



<!-- Card deck -->
<div class="card-deck centre w-50 m-auto">

    <!-- Card -->
    <div class="card mb-6 p-3 m-auto  text-white">
      <!--Card image-->
          <div><h1 class="card-title centre font-weight-bold">Modification email</h1></div>
          <hr>
          <p class="card-text m-auto">Adresse email : {{ Auth::user()->email }}</p>
          <form action="{{ route('compte.email' , Auth::user()->id) }}" method="POST" class="m-auto">
            @method('PATCH')
            @csrf
              <div>
                <label for="emailUpdate">Nouvelle adresse email:</label>
                <input type="text" class="@error('emailUpdate') is-invalid @enderror" id="emailUpdate" name="emailUpdate">
                @error('emailUpdate')
                <span class="invalid-feedback" role="alert">
                    <strong>Le champs email n'est pas valide</strong>
                </span>
                @enderror
              </div>
              <hr>
              <div class="centre">
                  <button type="submit" class="btn btn-primary">Valider</button>
              </div>
          </form>
    </div>
</div>

@endsection
