@extends('layouts/app')
@section('fond', 'register')
@section('content')


 <!-- Main navigation -->
<div class="container-fluid mt-3 mb-5">

    <!-- Full Page Intro -->
    <section>
      <!-- Mask & flexbox options-->
      <div class="mask d-flex justify-content-center align-items-center">
        <!-- Content -->
        <div class="container py-5 my-5">
          <!--Grid row-->
          <div class="row d-flex align-items-center justify-content-center">
            <!--Grid column-->
            <div class="col-md-6 col-xl-5">
              <!--Form-->
              <div class="card">
                <div class="card-body z-depth-2 px-4">

                <form method="POST" action="{{ route('register') }}">
                @csrf
                  <div class="md-form mt-3">
                    <i class="fa fa-user prefix grey-text"></i>
                    <label for="pseudo" class="col-form-label text-md-right">{{ __('Pseudo : ') }}</label>
                    <input id="pseudo" data-min="6" data-max="30" type="text"class="form-control @error('pseudo') is-invalid @enderror" name="pseudo" value="{{ old('pseudo') }}" required autocomplete="pseudo" autofocus>
                    @error('pseudo')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  <div class="md-form mt-3">
                    <i class="fa fa-envelope prefix grey-text"></i>
                    <label for="email" class="col-form-label text-md-right">{{ __('Adresse email : ') }}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  <div class="md-form mt-3">
                    <i class="fas fa-key prefix grey-text"></i>
                    <label for="password" class=" col-form-label text-md-right">{{ __('Mot de passe : ') }}</label>
                    <input id="password" type="password" data-min="8" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    <div class="text-center">
                        <p class="dark-grey-text mb-5 cache" id="erreur_password"></p>
                    </div>
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  <div class="md-form mt-3">
                    <i class="fas fa-key prefix grey-text"></i>
                    <label for="password-confirm" class=" col-form-label text-md-right">{{ __('Confirmer votre mot de passe :') }}</label>
                    <input id="password-confirm" type="password" data-min="8" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    <div class="text-center">
                        <p class="dark-grey-text mb-5 cache" id="erreur_confirm"></p>
                    </div>
                </div>

                  <div class="text-center mt-5 my-3">
                    <button id="bouton" class="btn btn-primary btn-block" disabled>
                        {{ __('S\'inscrire') }}
                    </button>
                </form>
                  </div>
                </div>
              </div>
              <!--/.Form-->
            </div>
            <!--Grid column-->
          </div>
          <!--Grid row-->
        </div>
        <!-- Content -->
      </div>
      <!-- Mask & flexbox options-->
    </section>
    <!-- Full Page Intro -->

  </div>
  <!-- Main navigation -->
  <script src="{{ asset('js/register.js') }}" defer></script>

@endsection
