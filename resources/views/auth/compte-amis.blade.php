@extends('layouts.app')
@section('fond', 'compte')
@section('content')

<div class="container w-25">
  <ul class="list-group list-group-flush cadre">
    <ul class="list-group">

        <h2 class="centre">Demande d'amis</h2>

        {{-- Condition if ne fonctionne pas à voir  --}}
        @if ($demandes)

            @foreach ($demandes as $demande)

            <hr>
                <li class="list-group-item list-group-item-default d-flex justify-content-between">
                    <a href="{{ route('amis.accepter', $demande->id ) }}" class="text-success">Accepter</a>
                    <a href="{{ route('voir-compte', $demande->pseudo ) }}" class="text-info">{{ $demande->pseudo }}</a>
                    <a href="{{ route('amis.refuser', $demande->id ) }}" class="text-danger">Refuser</a>
                </li>
            <hr>
            @endforeach

        @else

        <hr>
        <li>
            <h3 class="text-info centre">Aucune demande</h3>
        </li>

        @endif

    </ul>
  </ul>


<ul class="list-group list-group-flush cadre">
    <ul class="list-group">

        <h2 class="centre">Mes amis</h2>

        @foreach ($amis as $friends)
        <hr>
            <li class="list-group-item list-group-item-default d-flex justify-content-between">
                <a href="{{ route('voir-compte', $friends->pseudo ) }}" class="text-info">{{ $friends->pseudo }}</a>
                <a href="{{ route('amis-supprimer', $friends->pseudo ) }}" class="text-danger">Supprimer</a>
            </li>
        <hr>
        @endforeach

    </ul>
</ul>

</div>



@endsection
