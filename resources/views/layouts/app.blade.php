<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description" content="Site pour trouver des idées pour vos prochains voyages ainsi que des compagnons de voyage">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'IdéeDeVoyage') }}</title>

    <!-- Bootstrap / CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    {{-- script --}}
    <script src="{{ asset('js/app.js') }}" defer></script>
    {{-- <script src="https://use.fontawesome.com/42f91c3a73.js"></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap"
        rel="stylesheet">

</head>

<body id="@yield('fond')">

    <header class="sticky-top">
        <nav class="navbar navbar-expand-lg navbar-1 navbar-light menu">

            <a class="navbar-brand text-white" href="{{ route('accueil') }}">IdéeDeVoyage.fr</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>


            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('liste') }}">Liste des Pays</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="#">Annonces</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('comparateur') }}">Comparateur de Vol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('airbnb') }}">Trouver un Airbnb</a>
                        </li>
                    </ul>


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Se connecter') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Inscription') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                                {{-- si l'utilisateur à un avatar, c'est l'avatar qui s'affiche sinon son nom --}}
                                <?php
                                $user = Auth::user();

                                $oldfilename = $user->avatar;

                                $oldfileexists = Storage::disk('public')->exists($oldfilename);
                                ?>
                                @if ($oldfileexists)
                                <img src="/storage/{{ Auth::user()->avatar }}" alt="avatar du profil" class="avatar">
                                @else
                                {{-- Generation de l'avatar sur https://eu.ui-avatars.com/ --}}
                                <img src="https://eu.ui-avatars.com/api/?size=40&background=dfe9ee&rounded=true&name={{ Auth::user()->pseudo }}" alt="avatar du profil">
                                @endif

                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('compte') }}">Mon compte</a>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Déconnexion') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>

                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Recherche" aria-label="Search">
                        <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Rechercher</button>
                    </form>

        </nav>
    </header>

    @if(session()->has('message'))
    <div class="container w-25 alert alert-success">
        <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ session()->get('message') }}
    </div>
    @endif

    @yield('content')


    <!-- Footer -->
<footer class="pt-4">

    <div class="container justif-between">

      <div class="row">

        <div class="col-12 col-md-6 mt-5">

          <form class="form-inline col-12 col-md-12 centre">
            <input class="form-control form-control-sm mr-3 w-75" type="text" placeholder="Recherche"
              aria-label="Recherche">
            <button class="btn btn-outline-primary mt-3 col-4 col-md-4 centre" type="submit">Rechercher</button>
          </form>

        </div>

        <div class="col-md-3 mb-4 mt-2 centre">

                      <ul class="list-unstyled">
                          <li>
                              <a href="#!">À propos de IdéeDeVoyage.fr</a>
                          </li>
                          <li>
                              <a href="#!">Qui sommes-nous ?</a>
                          </li>
                          <li>
                              <a href="#!">Nos engagements</a>
                          </li>
                          <li>
                              <a href="#!">Conditions générales</a>
                          </li>
                      </ul>
        </div>

        <div class="col-md-3 mb-4 mt-2 centre">

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Politique de confidentialité</a>
                    </li>
                    <li>
                        <a href="#!">Politique de cookies</a>
                    </li>
                    <li>
                        <a href="#!">Moyens de paiements</a>
                    </li>
                    <li>

                        <a href="#!">Nous contacter</a>
                    </li>
                </ul>

        </div>

      </div>

    </div>
<hr>
    <div class="text-center py-3">
      <p>© 2020 Copyright IdéeDeVoyage.fr</p>
    </div>


  </footer>
  <!-- Footer -->


    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
</body>

</html>
