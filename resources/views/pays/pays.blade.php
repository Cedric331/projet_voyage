@extends('layouts/app')


@section('fond', 'france')

@section('content')

<div class="container cadre">
    <div class="row flex wrap">
        <div class="col-lg-6 centre">
            <img class="imagePays" src="{{ $pays->image_principal }}" alt="image {{ $pays->nom_pays }}">
        </div>
        <div class="col-lg-6 col-md-12">
            <h1>{{ $pays->nom_pays }}</h1>
            <h2>Continent : {{ $continent->nom_continent }}</h2>
            <h4>Budget moyen: {{ $pays->prix }} € / semaine</h4>
            <p>
                {{ $pays->description_pays  }}
            </p>
        </div>
    </div>


    <div class="mt-3 accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        Le climat ?
                    </button>
                </h2>
            </div>
            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    {{ $pays->description_climat }}
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Meilleure période pour partir ?
                    </button>
                </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                    {{ $pays->description_partir }}
                </div>
            </div>
        </div>
    </div>
</div>




<div class="container m-auto row">
    @foreach ( $tourismes as $tourisme )
        <div class="container mt-4 col-12 col-md-6 col-xl-4">
                  <div class="card">
                  <div class="card-body p-0">
                    <img src="{{ $tourisme->image_tourisme }}" class="card-img-top" alt="Image {{ $tourisme->titre_tourisme }}">
                    <h5 class="card-title m-2">{{ $tourisme->titre_tourisme }}</h5>
                    <hr>
                    <p class="card-text m-2">{{ $tourisme->description_tourisme }}</p>
                      <a href="{{ $tourisme->lien_tourisme }}" class="btn btn-primary m-2" target="blank">En savoir plus</a>
                  </div>
              </div>
          </div>
          @endforeach
</div>


@guest
<div class="container cadre centre">
    <p>Pour partager vos impressions sur ce Pays, vous devez être connecté</p>
</div>
@else
<div class="container cadre centre">
    <form action="{{ route('pays', $pays->id) }}" method="POST">
        @csrf
        <div class="form-group">
                <div>
                    <label for="commentaire">Un avis? Un témoignage? N'hésitez pas à partager avec nous </label>
                </div>
                <div>
                    <textarea class="@error('commentaire') is-invalid @enderror" name="commentaire" id="commentaire" cols="100" rows="10"></textarea>
                    @error('commentaire')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ "Votre commentaire ne peut pas dépasser 255 caractères" }}</strong>
                    </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Commenter</button>
        </div>
    </form>
</div>
@endguest

@foreach ($commentaires as $commentaire)

<div class="container cadre centre">
    <ul class="list-unstyled">
        <li class="media">
          <div class="media-body">

            <?php
            $oldfilename = $commentaire->avatar;

            $oldfileexists = Storage::disk('public')->exists($oldfilename);
            ?>
            @if ($oldfileexists)

            <h5 class="mt-0 mb-1">
                <a href="{{ route('voir-compte',  $commentaire->pseudo) }}"><img src="/storage/{{ $commentaire->avatar}}" alt="avatar du profil" class="avatar"> {{ $commentaire->pseudo }} : </a>
            </h5>
            @else
            {{-- Generation de l'avatar sur https://eu.ui-avatars.com/ --}}
            <h5 class="mt-0 mb-1">
                <a href="{{ route('voir-compte',  $commentaire->pseudo) }}"><img src="https://eu.ui-avatars.com/api/?size=40&background=dfe9ee&rounded=true&name={{ $commentaire->pseudo }}" alt="avatar du profil"> {{ $commentaire->pseudo }} : </a>
            </h5>
            @endif

            <p>{{ $commentaire->message_commentaire }}</p>
          </div>
        </li>
      </ul>
</div>
@endforeach

@endsection
