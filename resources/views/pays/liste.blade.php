@extends('layouts/app')

@section('fond', 'accueil')

@section('content')

<div class="container centre m-auto">
    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3">

    @foreach ($listes as $liste)

        <div class="col mb-4">
          <div class="card">

            <div class="view overlay">
              <img class="card-img-top" src="{{ $liste->image_principal }}" alt="{{ $liste->nom_pays }}">
            </div>

            <div class="card-body">
              <h2 class="card-title centre">{{ $liste->nom_pays }}</h2>
                <hr>
              <a href="{{ route('pays.pays', $liste->nom_pays) }}" class="centre btn btn-info">Voir ce Pays</a>
            </div>

          </div>
        </div>

    @endforeach

    </div>
</div>
@endsection
