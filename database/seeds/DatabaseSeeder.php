<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ContinentSeeder::class);
        $this->call(PaysSeeder::class);
        $this->call(TourismeSeeder::class);
        $this->call(CommentairePaysSeeder::class);
    }
}

