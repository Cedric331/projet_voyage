<?php

use App\Continents;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class ContinentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 6; $i++)
        {
           $continent = new Continents();

                $continent->nom_continent = Str::random(10);
                $continent->save();
        }
    }
}
