<?php

use App\Pays;
use App\User;
use App\CommentairePays;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class CommentairePaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 60; $i++)
        {
           $commentaire = new CommentairePays();

                $commentaire->message_commentaire = Str::random(50);
                $commentaire->pays_id = Pays::all()->random(1)->first()->id;
                $commentaire->user_id = User::all()->random(1)->first()->id;
                $commentaire->save();
        }
    }
}

