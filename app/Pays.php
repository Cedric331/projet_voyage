<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pays extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_pays',
        'prix',
        'image_Principal',
        'description_pays',
        'description_climat',
        'description_partir',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'created_at', 'update_at',
    ];

    /**
     * Récupère les commentaires du Pays concerné avec les utilisateures attachés aux commentaires
     *
     * @return void
     */
    public function commentaires()
    {
        return $this->hasMany(CommentairePays::class)
                ->join('users', 'users.id', 'user_id');
    }

    /**
     * Récupère les lieux touristiques du Pays
     *
     * @return void
     */
    public function tourismes()
    {
        return $this->hasMany(Tourismes::class);
    }

    /**
     * Récupère le continent du Pays
     *
     * @return void
     */
    public function continent()
    {
        return $this->belongsTo(Continents::class);
    }
}
