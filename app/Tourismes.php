<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tourismes extends Model
{
    protected $fillable = [
        'titre_Tourisme',
        'image_Tourisme',
        'description_tourisme',
        'lien_tourisme'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'update_at',
        ];

    public function pays()
    {
        return $this->belongsTo(Pays::class);
    }
}
