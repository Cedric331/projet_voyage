<?php

namespace App\Http\Controllers;

use App\Pays;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

    /**
     * Génère le formulaire pour créer un pays
     *
     * @return void
     */
    public function formCreerPays()
    {
        return view('admin.create-pays');
    }

    /**
     * Vérification et création d'un Pays
     *
     * @param Request $request
     * @return void
     */
    protected function creerPays()
    {
        $this->validate(request(), [
            'name' => ['required'],
            'prix' => ['required'],
            'imagePrincipal' => ['required'],
            'descriptionPrincipal' => ['required'],
            'descriptionClimat' => ['required'],
            'descriptionPartir' => ['required']
        ]);

         Pays::create(request([
            'name',
            'prix',
            'imagePrincipal',
            'descriptionPrincipal',
            'descriptionClimat',
            'descriptionPartir'
        ]));
        return redirect()->to('compte')->with('message', 'Pays enregistré');
    }


    /**
     * Retourne un tableau de l'ensemble des utilisateurs
     *
     * @return void
     */
    public function indexUser()
    {
        $users = User::paginate(15);

        return view('admin.users', ['users' => $users]);
    }

    /**
     * Supprime l'utilisateur qui correspond à l'id passé en paramètre
     *
     * @param [type] $id
     * @return void
     */
    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->back()->with('message', 'Utilisateur supprimé!');
    }

}
