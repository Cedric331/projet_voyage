<?php

namespace App\Http\Controllers;

use App\CommentairePays;
use App\Pays;
use App\User;
use App\Continents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Crypt;

class PaysController extends Controller
{

        /**
     * Affiche la liste des Pays par ordre croissant et avec une pagination de 9 éléments
     *
     * @return void
     */
    public function liste()
    {
        $pays = Pays::orderBy('nom_pays')->paginate(9);

        return view('pays/liste', ['listes' => $pays]);
    }

    /**
     * Retourne la page pays sélectionné ainsi que les tables qui ont un lien avec ce Pays
     *
     * @return void
     */
    public function affichePays($nomPays)
    {
        $pays = Pays::firstWhere('nom_pays', $nomPays);

        $pays = pays::find($pays->id);
        $continent = $pays->continent;
        $tourismes = $pays->tourismes;
        $commentaire = $pays->commentaires;

        return view('pays.pays', ['pays' => $pays, 'continent' => $continent, 'tourismes' => $tourismes, 'commentaires' => $commentaire]);
    }
}
