<?php

namespace App\Http\Controllers;

use App\CommentairePays;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentairePaysController extends Controller
{
    /**
     * Vérification du commentaire et ajout du commentaire en DB avant redirection
     * $request contient le commentaire envoyé par l'utilisateur authentifié et $idPays l'id du Pays
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function createCommentaire(Request $request, $idPays)
    {
            $request->validate([
                'commentaire' => 'required|max:255',
            ]);

            $commentaire = new CommentairePays;

            $commentaire->message_commentaire = $request->commentaire;

            $commentaire->user_id = Auth::user()->id;

            $commentaire->pays_id = $idPays;

            $commentaire->save();

            return redirect()->back()->with('message', 'Merci pour votre commentaire');
    }

}
