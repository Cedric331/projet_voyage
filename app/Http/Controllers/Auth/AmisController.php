<?php

namespace App\Http\Controllers;

use App\Amis;
use App\User;
use App\DemandeAmis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class AmisController extends Controller
{

    /**
     * Récupère en paramètre le pseudo de l'utilisateur sélectionné
     * Sélectionne l'utilisateur dans la DB et l'affiche sur la page voir-compte
     * Vérifie si une demande d'amis à déjà été envoyée
     * $value->selectDemande -> appel une fonction du model User
     * @param [type] $pseudo
     * @return void
     */
    public function voirCompte($pseudo)
    {
        // Récupération de l'utilisateur sélectionné avec le pseudo en paramètre
        $user = User::where('pseudo', $pseudo)->get();
        foreach ($user as $value)


        // Vérification si l'utilisateur à une demande d'amis avec l'utilisateur authentifié
        $verificationDemande1 = $value->selectDemande
                ->where('user_id', Auth::user()->id);

        // Récupération des informations de l'utilisateur authentifié
        $userAuth = User::findOrFail(Auth::user()->id);

        // Vérification si l'utilisateur authentifié à une demande d'amis avec l'utilisateur sélectionné
        $verificationDemande2 = $userAuth->selectDemande
                ->where('user_id', $value->id);

            // Si les vérifications renvoie des valeurs alors une demande est déjà en cours
            if (count($verificationDemande1) == 0 AND count($verificationDemande2) == 0)
            {
                $verif = false;
            }
            else
            {
                $verif = true;
            }



        // Vérification si l'utilisateur est amis avec l'utilisateur authentifié
        $verificationAmis1 = $value->amis
                                   ->where('ajout_id', Auth::user()->id);

        // Récupération des informations de l'utilisateur authentifié
        $userAuth = User::findOrFail(Auth::user()->id);

        // Vérification si l'utilisateur authentifié est amis avec l'utilisateur sélectionné
        $verificationAmis2 = $userAuth->amis
                                       ->where('ajout_id', $value->id);

        // Si les vérifications renvoie des valeurs alors les utilisateurs sont amis
        if (count($verificationAmis1) == 0 AND count($verificationAmis2) == 0)
        {
            $amis = false;
        }
        else
        {
            $amis = true;
        }

        return view('amis/voir-compte', ['utilisateurs' => $user, 'demandeEnCours' => $verif, 'amis' => $amis]);
    }

    /**
     * Retourne la page Amis avec les demandes en cours et les amis du compte de l'utilisateur
     * ($demandes) retoure les invitations d'ajout à la liste d'amis
     * ($amis) retourne les amis de l'utilisateur
     *
     * @return void
     */
    public function indexAmis()
    {
        $user = User::findOrFail(Auth::user()->id);

        $demandes = $user->demandeAmis;

        $amis = $user->amis;

        return view('auth/compte-amis', ['demandes' => $demandes, 'amis' => $amis]);
    }

    /**
     * Récupère l'id de l'utilisateur
     * Vérifie si l'id existe dans la DB, renvoie une erreur 404 en cas d'échec
     * et vérifie qu'une table n'existe pas déjà avec les mêmes informations
     * @param [type] $id
     * @return void
     */
    public function ajouterAmis($id)
    {
        $user = User::findOrFail($id);

        DemandeAmis::firstOrCreate(
            ['demandeEnAmis' => $user->id, 'user_id' => Auth::user()->id]
        );

        return redirect()->back()->with('message', 'Demande d\'amis envoyée');
    }

    /**
     * $id correspond à l'id de l'uitilisateur ayant effectué la demande d'ajout à la liste d'amis
     * la ligne de la table "DemandeAmis" concernée est supprimé de la DB
     *
     * @param [type] $id
     * @return void
     */
    public function refuser($id)
    {
        // Récupération de l'utilisateur auth qui refuse la demande d'amis
        $user = User::findOrFail(Auth::user()->id);

        // Sélection des demandes en cours concernant l'utilisateur auth
        // et récupération dans la liste de la demande qui possède user_id égale à $id
        // Retourne un array
        $selectDemande = $user->selectDemande
                              ->where('user_id', $id);

        // foreach dans le tableau pour récupérer les attributs et suppression de la demande via l'id
        foreach ($selectDemande as $value) {
            $delete = DemandeAmis::findOrFail($value->id);
            $delete->delete();
        }

        return redirect()->back()->with('message', 'Demande d\'amis refusée');
    }

        /**
     * $id correspond à l'id de l'uitilisateur ayant effectué la demande d'ajout à la liste d'amis
     * la ligne de la table "DemandeAmis" concernée est supprimé de la DB
     *
     * @param [type] $id
     * @return void
     */
    public function accepter($id)
    {
        $user = User::findOrFail(Auth::user()->id);

        $select = $user->selectDemande
                    ->where('user_id', $id);

        foreach ($select as $value)

        // Ajout dans la table amis des utilisateurs
            $amis = new Amis;
            $amis->amis = $user->id;
            $amis->ajout_id = $value->user_id;
            $amis->save();

            $amis = new Amis;
            $amis->amis = $value->user_id;
            $amis->ajout_id = $user->id;
            $amis->save();

            // Selection de la demande d'amis et suppression de celle-ci
            $delete = DemandeAmis::find($value->id);

            $delete->delete();


        return redirect()->back()->with('message', 'Demande d\'amis acceptée');
    }

    public function supprimerAmis($pseudo)
    {
        $userAuth =  User::findOrFail(Auth::user()->id);
        $userAmis = User::where('pseudo', $pseudo)->get();

        foreach($userAmis as  $value)

       $amis = Amis::where([['ajout_id', $value->id], ['amis', $userAuth->id]]);
       $amis->delete();

       $amis = Amis::where([['amis', $value->id], ['ajout_id', $userAuth->id]]);
       $amis->delete();


        return redirect()->back()->with('message', 'Amis supprimé');
    }
}
