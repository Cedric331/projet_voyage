<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pseudo', 'email', 'avatar', 'password', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'update_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Renvoi les commentaires de l'utilisateur
     *
     * @return void
     */
    public function commentaires()
    {
        return $this->hasMany(CommentairePays::class);
    }

    /**
     * récupère les demandes d'ajout d'amis de l'utilisateur
     * demandeEnAmis correspond à l'id de l'utilisateur concernée par la demande
     * et la requête va récupérer l'utilisateur à l'initiative de la demande d'ajout
     *
     * @return void
     */
    public function demandeAmis()
    {
        return $this->hasMany(DemandeAmis::class, 'demandeEnAmis')
                ->join('users', 'users.id', '=', 'demande_amis.user_id');
    }

    /**
     * Sélectionne dans la table "DemandeAmis" les demandes concernant l'utilisateur
     *
     * @return void
     */
    public function selectDemande()
    {
        return $this->hasMany(DemandeAmis::class, 'demandeEnAmis');
    }

    /**
     * Sélectionne dans la table "DemandeAmis" les demandes concernant l'utilisateur
     *
     * @return void
     */
    public function verifAmis()
    {
        return $this->hasMany(Amis::class, 'demandeEnAmis');
    }

    /**
     * Retourne les amis de l'utilisateur
     *
     * @return void
     */
    public function amis()
    {
        return $this->hasMany(Amis::class, 'amis')
                ->join('users', 'users.id', '=', 'amis.ajout_id');
    }
}
