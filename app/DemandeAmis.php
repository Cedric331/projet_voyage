<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class DemandeAmis extends Model
{
    protected $fillable = [
        'demandeEnAmis',
        'user_id'
    ];

}
