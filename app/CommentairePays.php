<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentairePays extends Model
{
    protected $fillable = [
        'message_commentaire',
    ];

    /**
     * Récupère le Pays du commentaire
     *
     * @return void
     */
    public function pays()
    {
        return $this->belongsTo(Pays::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
