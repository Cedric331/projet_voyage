<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amis extends Model
{
    protected $fillable = [
        'amis',
        'user_id'
    ];
}
