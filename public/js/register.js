var inputMail = document.querySelector('input[type=email]');
var inputPseudo = document.querySelector('#pseudo');
var inputPassword = document.querySelector('#password');
var inputConfirm_Password = document.querySelector('#password-confirm');
var erreurPassword = document.querySelector('#erreur_password');
var erreurConfirmPassword = document.querySelector('#erreur_confirm');


inputMail.addEventListener('keyup', verificationMail);

inputPseudo.addEventListener("keyup", verificationPseudo);

inputPassword.addEventListener("keyup", verificationPassword);

inputConfirm_Password.addEventListener("keyup", verificationConfirmPassword);



function verificationMail()
{
    if(inputMail.validity.typeMismatch) {
        inputMail.setCustomValidity("Merci de renseigner un email valide !");
        this.classList.add("incorrect");
        this.classList.remove("correct");
      } else {
        inputMail.setCustomValidity("");
        this.classList.remove("incorrect");
        this.classList.add("correct");
      }
      verificationBouton();
}

function verificationPseudo()
{
    if(this.value.length < this.dataset.min || this.value.length > this.dataset.max)
    {
        inputPseudo.setCustomValidity("Votre pseudo doit contenir au moins 6 caractères !");
        this.classList.add("incorrect");
        this.classList.remove("correct");
    }
    else
    {
      inputPseudo.setCustomValidity("");
      this.classList.remove("incorrect");
      this.classList.add("correct");
    }
      verificationBouton();
}

function verificationPassword()
{
    if(this.value.length < this.dataset.min)
    {
        erreur_password.textContent = "Le mot de passe doit contenir au moins 8 caractères !";
        erreur_password.classList.remove("cache");
        this.classList.add("incorrect");
        this.classList.remove("correct");
    }
    else
    {
        erreur_password.textContent = "";
        erreur_password.classList.add("cache");
        inputPassword.setCustomValidity("");
        this.classList.remove("incorrect");
        this.classList.add("correct");
    }
}

function verificationConfirmPassword()
{
    if (this.value.length < this.dataset.min && inputPassword.value != this.value)
    {
        erreurConfirmPassword.textContent = "Le mot de passe doit être identique !";
        erreurConfirmPassword.classList.remove("cache");
        this.classList.add("incorrect");
        this.classList.remove("correct");
    }
    else
    {
        erreurConfirmPassword.textContent = "";
        erreurConfirmPassword.classList.add("cache");
        this.classList.add("correct");
        this.classList.remove("incorrect");
    }
      verificationBouton();
}

function verificationBouton()
{
    var valide = document.querySelectorAll(".correct");
    // Vérifie si il y a 2 éléments qui ont la classe "correct"
    if (valide.length == 4)
    {
       // Si ok supprime l'attribut disabled du bouton pour valider le formulaire
       document.querySelector("#bouton").removeAttribute("disabled");
    }
    else
    {
       // Si il y a moins de deux éléments avec la classe "correct", ajoute l'attribut "disabled" sur le bouton
       document.querySelector("#bouton").setAttribute("disabled", "");
    }
}
