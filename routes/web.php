<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route générales

// Page d'accueil
Route::view('/','accueil');

//Route home
// Route::get('/home', 'HomeController@index')->name('home');

// Affiche page d'accueil
Route::view('accueil', 'accueil')->name('accueil');

// Route Pays

// Affiche liste des pays
Route::get('/liste', 'PaysController@liste')->name('liste');

// Affiche le pays sélectionné
Route::get('/pays/{nomPays}', 'PaysController@affichePays')->name('pays.pays');

// Affiche le Comparateur de Vol
Route::get('/comparateur', 'ComparateurController@indexComparateur')->name('comparateur');

// Affiche le Comparateur de Vol
Route::get('/airbnb', 'ComparateurController@indexAirbnb')->name('airbnb');


// Authentification
Auth::routes();

Route::group([
    'middleware' => 'auth',
], function () {

// Affiche page du compte
Route::get('compte', 'CompteController@index')->name('compte');

// Affiche formulaire de mise à jour de l'adresse email
Route::get('compte/email', 'CompteController@emailUpdateForm')->name('compte-email');

// Mettre à jour l'adresse email
Route::patch('compte/email/{id}', 'CompteController@emailUpdate')->name('compte.email');

// Supprimer le compte de l'utilisateur
Route::delete('compte/{id}/supprimer', 'CompteController@destroy')->name('compte.supprimer');

// mettre à jour l'Avatar
Route::patch('compte/avatar/{id}', 'CompteController@avatarUpdate')->name('compte.avatar');

// Poster un commentaire sur la page Pays
Route::post('/pays/{id}', 'CommentairePaysController@createCommentaire')->name('pays');

//Voir le compte d'un autre utilisateur
Route::get('/voir-compte/{pseudo}', 'AmisController@voirCompte')->name('voir-compte');

// Demande d'ajout d'un ami
Route::get('/voir-compte/ajouter/{id}', 'AmisController@ajouterAmis')->name('ajouter.amis');

// Affiche page amis
Route::get('/compte/amis', 'AmisController@indexAmis')->name('compte-amis');

//Demande d'amis rejetée
Route::get('/compte/amis/refuser/{id}', 'AmisController@refuser')->name('amis.refuser');

//Demande d'amis acceptée
Route::get('/compte/amis/accepter/{id}', 'AmisController@accepter')->name('amis.accepter');

// Supprime un amis
Route::get('/compte/amis/{id}/supprimer', 'AmisController@supprimerAmis')->name('amis-supprimer');

});

// Admin
Route::group([
    'middleware' => 'is_admin',
], function () {
    // Affichage du formulaire de création d'un pays
    Route::get('/compte/admin/creer/pays', 'AdminController@formCreerPays')->name('creer-pays');

    // Création d'un pays
    Route::post('/compte/admin/creer/pays', 'AdminController@creerPays')->name('creation-pays');

    //Affiche la liste des utilisateurs
    Route::get('/compte/admin/utilisateurs', 'AdminController@indexUser')->name('users');

    //Supprimer un utilisateur
    Route::delete('/compte/admin/utilisateurs/delete/{id}', 'AdminController@deleteUser')->name('users-delete');
});

